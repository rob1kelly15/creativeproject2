$(document).ready(function() {

		var myurl= "https://swapi.co/api/films";
		$.ajax({
			url : myurl,
			dataType : "json",
			success : function(json) {
				var results = "";
				results += '<h2>All Movies</h2>';
				results += '<div class="movieTitles">'
				for (var i=0; i<json.results.length; i++) {
					results += '<button class="movieBtn" value="'+ json.results[i].url + '">' + json.results[i].title + '</button>'
				}
				$("#movieResults").html(results);
				
			}
		});
		
		
		$(".movieOne").click(function(e) {
			var myurl= "https://swapi.co/api/films/4";
			getInfo(myurl);
		});
		$(".movieTwo").click(function(e) {
			var myurl= "https://swapi.co/api/films/5";
			getInfo(myurl);
		});
		$(".movieThree").click(function(e) {
			var myurl= "https://swapi.co/api/films/6";
			getInfo(myurl);
		});
		$(".movieFour").click(function(e) {
			var myurl= "https://swapi.co/api/films/1";
			getInfo(myurl);
		});
		$(".movieFive").click(function(e) {
			var myurl= "https://swapi.co/api/films/2";
			getInfo(myurl);
		});
		$(".movieSix").click(function(e) {
			var myurl= "https://swapi.co/api/films/3";
			getInfo(myurl);
		});
		$(".movieSeven").click(function(e) {
			var myurl= "https://swapi.co/api/films/7";
			getInfo(myurl);
		});
		

			


		function getInfo(myurl) {
			$.ajax({
				url : myurl,
				dataType : "json",
				success : function(json) {
					$(".directorProducer").html(json.director + " and " + json.producer);
					$('.characters').html('');
					$('.planets').html('');
					$('.starships').html('');
					$('.vehicles').html('');
					$('.species').html('');
					
					for(var i = 0; i < json.characters.length; i++) {
						characterInfo(json.characters[i]);
					}


					for(var i = 0; i < json.planets.length; i++) {
						planetInfo(json.planets[i]);
					}

					for(var i = 0; i < json.starships.length; i++) {
						starshipInfo(json.starships[i]);
					}

					for(var i = 0; i < json.vehicles.length; i++) {
						vehicleInfo(json.vehicles[i]);
					}
					for(var i = 0; i < json.species.length; i++) {
						speciesInfo(json.species[i]);
					}
										
					
						
				}
			});
		}

		function characterInfo(myUrl) {
			var charResults = "";
			$.ajax({
				url : myUrl,
				dataType : "json",
				success : function(characterJson) {
					charResults = characterJson.name;
					$('.characters').append('<p>' + charResults + '</p>');
				}
			});
		}

		function planetInfo(myUrl) {
			var charResults = "";
			$.ajax({
				url : myUrl,
				dataType : "json",
				success : function(planetJson) {
					charResults = planetJson.name;
					$('.planets').append('<p>' + charResults + '</p>');
				}
			});
		}

		function starshipInfo(myUrl) {
			var charResults = "";
			$.ajax({
				url : myUrl,
				dataType : "json",
				success : function(starshipJson) {
					charResults = starshipJson.name;
					$('.starships').append('<p>' + charResults + '</p>');
				}
			});
		}

		function vehicleInfo(myUrl) {
			var charResults = "";
			$.ajax({
				url : myUrl,
				dataType : "json",
				success : function(vehicleJson) {
					charResults = vehicleJson.name;
					$('.vehicles').append('<p>' + charResults + '</p>');
				}
			});
		}

		function speciesInfo(myUrl) {
			var charResults = "";
			$.ajax({
				url : myUrl,
				dataType : "json",
				success : function(speciesJson) {
					charResults = speciesJson.name;
					$('.species').append('<p>' + charResults + '</p>');
				}
			});
		}
	});